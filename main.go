package main

import (
	"encoding/json"
	"net/http"
)

// Base list of available url paths
type Base struct {
	Options []string `json:"options"`
}

// Health ERROR or OK status
type Health struct {
	Status string `json:"status"`
}

func main() {
	// Create a basic http example to demonstrate example
	http.Handle("/", http.HandlerFunc(BasicHandler))
	http.Handle("/health", http.HandlerFunc(HealthHandler))
	http.ListenAndServe(":8080", nil)
}

// HealthHandler Return {"status": "OK"}
func HealthHandler(w http.ResponseWriter, r *http.Request) {
	health := Health{"OK"}
	res, err := json.Marshal(health)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(res)
}

// BasicHandler Return {"options": ["/health", "/"]}
func BasicHandler(w http.ResponseWriter, r *http.Request) {
	base := Base{[]string{"/", "health"}}
	res, err := json.Marshal(base)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(res)
}
