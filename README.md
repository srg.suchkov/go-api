# go-api

# Simple Golang API for Test perspective 

### Build
```
cd {{ project_dir }}
go build -o api-example
```
Will cteate `api-example` file in {{ project_dir }}

### Run
```
./api-example

curl -s localhost:8080
```
